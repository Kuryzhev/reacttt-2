import React, {useState} from 'react';
import './App.css';
import {
  Switch,
  Route,
} from "react-router-dom";
import Todos from './components/todos/todos';
import Todo from './components/todo/todo';
import Modal from './components/modal/modal';
import {useHistory, withRouter} from 'react-router';

function App() {
  const history = useHistory();

  const checkAuth = () => {
    if (localStorage.getItem('Authorized') === null) {
      return true;
    } else {
      if (history.location.pathname !== '/') return false;

      history.push("/todos");
      return false;
    }
  }
  const [open, setOpen] = useState(checkAuth());
  const [login, setLogin] = useState('');

  const submitLogin = () => {
    if (login.length === 0) return;
    localStorage.setItem('Authorized', login);
    history.push("/todos");
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  }

  const onChangeField = (value) => {
    setLogin(value);
  }

  return (
    <div className="App">
      <Modal
        isOpen={open}
        onHandleClose={handleClose}
        onChangeField={onChangeField}
        onSubmitLogin={submitLogin}
      />
        <Switch>
          <Route path="/todos">
            <Todos />
          </Route>
          <Route path="/todos:id">
            <Todo />
          </Route>
        </Switch>
    </div>
  );
}

export default withRouter(App);
