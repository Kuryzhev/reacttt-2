import axios from 'axios';
import {apiUrl} from '../constants';

export const TODOS_PENDING = 'TODOS_PENDING';
export const TODOS_SUCCESS = 'TODOS_SUCCESS';
export const TODOS_FAILURE = 'TODOS_FAILURE';

export const TODO_PENDING = 'TODO_PENDING';
export const TODO_SUCCESS = 'TODO_SUCCESS';
export const TODO_FAILURE = 'TODO_FAILURE';

export const getTodosAsync = () => dispatch => {
  dispatch({
    type: TODOS_PENDING,
    payload: []
  });

  axios.get(`${apiUrl}/todos`)
    .then(response => {
      dispatch({
        type: TODOS_SUCCESS,
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: TODOS_FAILURE,
        payload: []
      });
    });
}

export const getTodoAsync = (todoId) => dispatch => {
  dispatch({
    type: TODO_PENDING,
    payload: {}
  });

  axios.get(`${apiUrl}/todos/${todoId}`)
    .then(response => {
      dispatch({
        type: TODO_SUCCESS,
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: TODO_FAILURE,
        payload: []
      });
    });
}
