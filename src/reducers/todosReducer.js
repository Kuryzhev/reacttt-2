import { TODOS_PENDING, TODOS_SUCCESS, TODOS_FAILURE } from '../actions';

const initialState = {
  loading: false,
  data: []
}

export default function storeTodos(state = initialState, action) {
  switch (action.type) {
    case TODOS_PENDING:
      return {
        ...state,
        loading: true,
        data: action.payload
      };
    case TODOS_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload
      };
    case TODOS_FAILURE:
      return {
        ...state,
        loading: false,
        data: action.payload
      };
    default:
      return state;
  }
}
