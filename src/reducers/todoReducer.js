import { TODO_PENDING, TODO_SUCCESS, TODO_FAILURE } from '../actions';

const initialState = {
  loading: false,
  data: {}
}

export default function storeTodos(state = initialState, action) {
  switch (action.type) {
    case TODO_PENDING:
      return {
        ...state,
        loading: true,
        data: action.payload
      };
    case TODO_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload
      };
    case TODO_FAILURE:
      return {
        ...state,
        loading: false,
        data: action.payload
      };
    default:
      return state;
  }
}
