import React, { useEffect } from 'react';
import './todos.css';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import { getTodosAsync } from '../../actions';
import { Link } from "react-router-dom";
import CircularProgress from '@material-ui/core/CircularProgress';
import {useHistory} from 'react-router';

function Todos(state) {
  const history = useHistory();

  useEffect(() => {
    state.getTodosAsync();
  }, []);

  const signOut = () => {
    localStorage.clear();
    history.push("/");
  }

  if (state.loading) {
    return <CircularProgress />;
  }

  return (
    <ul className="Todos">
      <Button variant="contained" color="secondary" onClick={() => signOut()}>
        Sign out
      </Button>
      <h1 className="title">Todos:</h1>
      {state.data.map(item =>
        <li className="Todos-item" key={`${item.id}${item.userId}`}>
          <Link className="Todos-item-link" to={`todos:${item.id}`}>{item.title}</Link>
          <span className={`${item.completed ? 'success' : 'failure'}`}></span>
        </li>
      )}
    </ul>
  );
}

const mapStateToProps = (state) => {
  return {
    data: state.todos.data,
    loading: state.todos.loading
  }
}

const mapDispatchToProps = { getTodosAsync }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Todos);
