import React from 'react';
import './modal.css';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function Modal({
  isOpen,
  onHandleClose,
  onChangeField,
  onSubmitLogin
}) {
  return (
    <Dialog open={isOpen} onClose={onHandleClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Login</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.
        </DialogContentText>
        <TextField
          onChange={(e) => onChangeField(e.target.value)}
          autoFocus
          margin="dense"
          id="name"
          label="Login:"
          type="text"
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onSubmitLogin} color="primary">
          Signin
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default Modal;
