import React, { useEffect } from 'react';
import './todo.css';
import {connect} from 'react-redux';
import { useParams } from "react-router-dom";
import { getTodoAsync } from '../../actions';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link } from "react-router-dom";

function Todo(state) {
  let { id } = useParams();
  const todoId = id.replace(':', '');

  useEffect(() => {
    state.getTodoAsync(todoId);
  }, []);

  if (state.loading) {
    return <CircularProgress />
  }

  return [
    <Link key="nav-todo" className="back-link" to="/todos">Back</Link>,
    <h3 key="title-todo">Todo item:</h3>,
    <ul key="list-todo" className="Todo">
      <li>UserId: {state.data.userId}</li>
      <li>TodoId: {state.data.id}</li>
      <li>Title: {state.data.title}</li>
      <li>Status: <span className={`${state.data.completed ? 'success' : 'failure'}`}></span></li>
    </ul>
  ];
}

const mapStateToProps = (state) => {
  return {
    data: state.todo.data,
    loading: state.todo.loading
  }
}

const mapDispatchToProps = { getTodoAsync }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Todo);
